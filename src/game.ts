import {BuilderHUD} from './modules/BuilderHUD'

const scene = new Entity()
const transform = new Transform({
  position: new Vector3(0, 0, 0),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
scene.addComponentOrReplace(transform)
engine.addEntity(scene)

const floorBaseSand_01 = new Entity()
floorBaseSand_01.setParent(scene)
const gltfShape = new GLTFShape('models/FloorBaseSand_01/FloorBaseSand_01.glb')
floorBaseSand_01.addComponentOrReplace(gltfShape)
const transform_2 = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseSand_01.addComponentOrReplace(transform_2)
engine.addEntity(floorBaseSand_01)

const vase_06 = new Entity()
vase_06.setParent(scene)
const gltfShape_2 = new GLTFShape('models/Vase_06/Vase_06.glb')
vase_06.addComponentOrReplace(gltfShape_2)
const transform_3 = new Transform({
  position: new Vector3(3, 0, 11.5),
  rotation: new Quaternion(0, 0.7071067811865475, 0, 0.7071067811865475),
  scale: new Vector3(4, 4, 4)
})
vase_06.addComponentOrReplace(transform_3)
engine.addEntity(vase_06)

const bonesSkull_01 = new Entity()
bonesSkull_01.setParent(scene)
const gltfShape_3 = new GLTFShape('models/BonesSkull_01/BonesSkull_01.glb')
bonesSkull_01.addComponentOrReplace(gltfShape_3)
const transform_4 = new Transform({
  position: new Vector3(2, 0, 2.5),
  rotation: new Quaternion(0, -0.9569403357322092, 0, -0.29028467725446244),
  scale: new Vector3(4, 4, 4)
})
bonesSkull_01.addComponentOrReplace(transform_4)
engine.addEntity(bonesSkull_01)

const reef = new Entity()
reef.setParent(scene)
const reefShape_4 = new GLTFShape('models/reef/reef.glb')
reef.addComponentOrReplace(reefShape_4)
const transform_5 = new Transform({
  position: new Vector3(8, 5.3, 8),
  rotation: new Quaternion(0, 0, 0, 10),
  scale: new Vector3(6, 6, 6)
})
reef.addComponentOrReplace(transform_5)
engine.addEntity(reef)

const hud:BuilderHUD =  new BuilderHUD()
hud.setDefaultParent(scene)
hud.attachToEntity(bonesSkull_01)
hud.attachToEntity(vase_06)
hud.attachToEntity(reef)